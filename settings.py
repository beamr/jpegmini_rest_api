#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


import bottle
import os

bottle.Request.MEMFILE_MAX = 1024*1024*25

if os.getenv('JPEGMINI_API_DISABLE_REDIS'):
    REDIS_SETTINGS_DB = None
else: 
    REDIS_SETTINGS_DB = {'host': 'localhost', 'port': 6379, 'db': 0, 'password': None}

HTPASSWORD_FILE = '/opt/jpegmini/.htpasswd'
RESTART_SCRIPT = '/opt/jpegmini/sbin/restart_web_servers.sh'
