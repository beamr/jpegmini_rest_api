#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


import urlparse
import requests
import re


def cmp_versions(version1, version2):
    """
    Helper method to compare two version strings
    inspired by:
    http://stackoverflow.com/questions/1714027/version-number-comparison
    """
    def normalize(v):
        return [int(x) for x in re.sub(r'[^\.\d]', '.', v).split('.') if x]
    return cmp(normalize(version1), normalize(version2))


def check_latest_version():
    """
    Get latest version numbers of jpegmini from jpegmini.com
    """
    try:
        response = requests.get("http://www.jpegmini.com/app_services/aws/get_latest_version", timeout=3)
        res = response.json()
    except:
        res = {'jpegmini': None, 'api': None}

    return res


def get_current_jm_api_version():
    return '1.0'


def get_ec2_meta(path):
    """
    Retrieve data from the ec2 meta-data server
    If the server doesn't answer (development machine), return 'ERROR'
    """
    try:
        res = requests.get(urlparse.urljoin('http://169.254.169.254', path), timeout=0.1)
        if res.status_code == 200:
            return res.content
        else:
            return 'ERROR'
    except:
        return 'ERROR'


def seconds_to_human(seconds):
    seconds = int(seconds)
    if seconds <= 0: return ''
    days = seconds // 86400
    seconds %= 86400
    hours = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    res = '%ds' % seconds if seconds else ''
    if minutes: res = '%dm, %s' % (minutes, res)
    if hours: res = '%dh, %s' % (hours, res)
    if days: res = '%dd, %s' % (days, res)
    if res.endswith(', '):
        res = res[:-2]
    return res
