#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import settings

class MockPreferences(object):
   def get(self, key, default=None):
       return default
   def set(self, *args, **kwargs):
       return None
   def incr(self, *args, **kwargs):
       return None

class RedisPreferences(object):
    def __init__(self):
        import redis
        self._redis = redis.StrictRedis(
            host=settings.REDIS_SETTINGS_DB['host'],
            port=settings.REDIS_SETTINGS_DB['port'],
            db=settings.REDIS_SETTINGS_DB['db'],
            password=settings.REDIS_SETTINGS_DB['password'])

    def get(self, key, default=None):
        return self._redis.hget('jpegmini_api_settings', key) or default

    def set(self, key, value):
        return self._redis.hset('jpegmini_api_settings', key, value)

    def remove(self, key):
        """
        Remove a key from the db
        """
        return  self._redis.hdel('jpegmini_api_settings', key)

    def list(self):
        """
        retrieve all config keys and values
        """
        return self._redis.hgetall('jpegmini_api_settings')

    def incr(self, key, delta=1):
        """
        increase counter by delta
        """
        self._redis.hincrby('jpegmini_api_settings', key, delta)

if settings.REDIS_SETTINGS_DB == None:
    Preferences = MockPreferences
else:
    Preferences = RedisPreferences

