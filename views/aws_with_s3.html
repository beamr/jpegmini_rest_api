{% extends "doc_template.html" %}
{% block doc_content %}
    <h1>Tutorial 2: Using JPEGmin for AWS with S3</h1>
    <h3>Move photos back and forth between Amazon S3 and your JPEGmini for AWS instance</h3>
    <h2>Overview</h2>
    <p>In this tutorial, you will learn how to copy photos from Amazon S3 to your JPEGmini Photo Server running on AWS, optimize them, and copy the optimized photos back to S3. “Amazon S3 is the storage for the internet”, therefore a simple method of processing photos stored on s3 is extremely valuable. For more details about S3, see <a href="http://aws.amazon.com/s3/" target="_blank">the S3 home page.</a></p>

    <h2>Prerequisites:</h2>
    <p>
    <ol>
        <li>A running instance of the JPEGmini Photos Server - See tutorial <a href="/docs/tutorials/start">here</a></li>
    </ol>
    </p>

    <h2>Five Simple Steps:</h2>
    <ol>
        <li><a href="#step1">Setup access credentials for your JPEGmini instance</a></li>
        <li><a href="#step2">Create an S3 bucket, and place some photos in it</a></li>
        <li><a href="#step3">Copy the original photos from your S3 bucket to your JPEGmini Instance</a></li>
        <li><a href="#step4">Optimize your photos</a></li>
        <li><a href="#step5">Copy the optimized photos back to your S3 bucket</a></li>
    </ol>
    </p>

    <h3 id="step1">Step 1. Setup access credentials for your JPEGmini instance</h3>
    <p>Before accessing S3 from your running instance on AWS, you will need to setup the appropriate access credentials. AWS supports user-based access tokens, groups and roles. In this tutorial we will create a dedicated user for the JPEGmini Photo Server instance, and grant that user full access to S3.</p>
    <p>Note that on a production environment, you would probably want more restrictive access for increased security. For additional information on access management, see the <a href="http://aws.amazon.com/iam/" target="_blank">IAM documentation</a>.</p>
    <p>
    To create the user, follow these steps:
    <ol>
        <li>Login to your AWS console and switch to the IAM service: <br /><a href="https://console.aws.amazon.com/iam/home" target="_blank">https://console.aws.amazon.com/iam/home</a>
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Management-Console-Home.png' />
        </li>
        <li>Select the Users tab (on the left), and click on “Create New User”</li>
        <li>In the “Create User” dialog, enter just a single name: “jpegmini”, and click on “Create”
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Management-Console-user.png' />
        </li>
        <li>Click on “Download Credentials” to download a file containing the access id and secret access key for this new user.</li>
        <li>Close the dialog</li>
    </ol>
    </p>

    <p>
    To grant the jpegmini user full access to S3, follow these steps:
    <ol>
        <li>From the IAM console, select the jpegmini user</li>
        <li>Open the Permissions tab (bottom section of the screen), and click “Attach User Policy”</li>
        <li>Select “Amazon S3 Full Access”, click “Select”, and then click “Apply Policy”
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Management-Console-User-S3-Access.png' />
        </li>

    </ol>
    </p>

    <h3 id="step2">Step 2. Create an S3 bucket, and place some photos in it</h3>
    <p>There are various tools to manage S3, create buckets, and transfer files between S3 and your computer (e.g. <a href="http://aws.amazon.com/console/" target="_blank">AWS Console</a>, <a href="http://s3browser.com/" target="_blank">S3browser</a>, etc). In this tutorial we use the AWS console to create a bucket and upload some files to it. For additional information about S3, see the Amazon <a href="http://aws.amazon.com/s3/" target="_blank">S3 documentation</a>.</p>
    <p>
    To create an S3 bucket for your photos, follow these steps:
    <ol>
        <li>Login to the <a href="https://console.aws.amazon.com/" target="_blank">S3 console</a></li>
        <li>Click on the “Create Bucket” button on the top left</li>
        <li>In the dialog, set the bucket name to any unique name and click on “Create”
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/Aws-Console-S3-Create-Buckets.png' />
        </li>
    </ol>
    </p>

    <p>Note the bucket names must be globally unique (i.e. two users cannot have the same bucket names). Repeat the three previous steps to create a second bucket with a different name to hold the optimized photos.</p>
    <p>
    To upload photos to your S3 bucket, follow these steps in the AWS console:
    <ol>
        <li>Double click on the bucket name to enter the bucket
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Console-S3-Empty.png' />
        </li>
        <li>Click on the blue “Upload” button to open the upload dialog</li>
        <li>Click on Add Files, and select a few jpeg photos from your computer</li>
        <li>To make the files publicly accessible for download, you can click on “Set Details”, followed by “Set Permissions”, and check “Make everything public”
            <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Console-S3-Make-Public2.png' />
        </li>
        <li>Click on “Start Upload” to upload your photos</li>
    </ol>
    </p>
    <p>By making the files public, you can view each photo directly from your browser. Get the file URL for one of the photos by right-clicking on it and selecting “Properties” from the context menu. The link to the file is shown on the right.
        <img src='http://s3.amazonaws.com/jpegminimedia/images/aws_v1/AWS-Console-S3-Download.png' />
    </p>

    <h3 id="step3">Step 3. Copy the original photos from your S3 bucket to your JPEGmini Instance</h3>
    <p>In this tutorial we use <a href="http://s3tools.org/s3cmd" target="_blank">s3cmd</a> to copy files between S3 and the JPEGmini Photo Server instance. s3cmd is pre-installed on the JPEGmini Photo Server instance. It is a simple command line tool that can easily be automated (scripted) to manage all S3 file transfers.</p>
    <p>Before using s3cmd for the first time, you will need to configure it with the credentials generated in the first step.  Login to your JPEGmini instance, and run the following command:</p>
    <pre>s3cmd --configure</pre>
    <p>Copy the Access Key and Secret Key from the credentials.csv file which was downloaded in the previous step. Use the default answer for all other items, and when prompted to “Save Settings”, answer “Yes”.</p>
    <p>To list your S3 buckets and files use the following commands:</p>
    <pre>s3cmd ls<br/>s3cmd ls s3://<em>your_back_name</em></pre>
    <p>To copy files or folders from S3 to your instance, use the get command:</p>
    <pre>s3cmd get s3://<em>your_back_name/your-photo-name</em><br/>s3cmd get --recursive s3://<em>your-bucket-name</em>/</pre>
    <p>Copy the original photos from your S3 bucket into a folder called <em>~/my_photos</em> on your running instance:</p>
    <pre>cd<br/>mkdir my_photos<br/>cd my_photos<br/>s3cmd get --recursive s3://<em>your-bucket-name</em>/<br/>ls -l</pre>
    <p>You should now see a list of your photo files, which are now located on the server.</p>

    <h3 id="step4">Step 4. Optimize your photos</h3>
    <p>Change back to your home folder, and run JPEGmini as follows (depending on the number of photos this may take some time):</p>
    <pre>cd<br/>mkdir my_optimized_photos<br/>jpegmini -f=my_photos -o=my_optimized_photos<br/>ls -l my_optimized_photos</pre>
    <p>You should now see a list of files (your optimized photos). Note the files should have a _mini suffix in their filename, and their sizes should be smaller than the original files.</p>

    <h3 id="step5">Step 5. Copy the optimized photos back to your S3 bucket</h3>
    <p>The last step is to upload the optimized photos back to S3. Use the following command to run s3cmd and upload the optimized photos:</p>
    <pre>s3cmd put -P ~/my_optimized_photos/* s3://<em>your-2nd-bucket-name</em></pre>
    <p>The -P flag is used to make the files publicly available for download.<br/>Go back to the S3 Console to verify that the optimized photos have been placed in the bucket.</p>
{% endblock %}
