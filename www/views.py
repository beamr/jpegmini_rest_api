#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import bottle
from bottle import jinja2_view as view
import socket
from api import prefs
from common import utils
from bottle import error


def default_context():
    return {
        'STATIC_URL': '/static'
    }


@bottle.route('/static/:filename#.*#')
def send_static(filename):
    return bottle.static_file(filename, root='./static/')


@bottle.route('/', method='GET')
@view('home.html')
def get_home():
    context = dict(default_context())
    context.update({
        'TOTAL_PHOTOS': prefs.get('total_photos', ''),
        'TOTAL_SAVING': prefs.get('total_saving', '')})
    return context

@bottle.route('<:re:/docs/overview/?>', method='GET')
@bottle.route('<:re:/docs>', method='GET')
@view('overview.html')
def get_overview():
    context = dict(default_context())
    context.update({'PAGE_ID': 'overview'})
    return context


@bottle.route('<:re:/docs/cmd/overview?>', method='GET')
@view('cmd_overview.html')
def get_cmd_overview():
    context = dict(default_context())
    context.update({'PAGE_ID': 'cmd_overview'})
    return context


@bottle.route('<:re:/docs/cmd/ref?>', method='GET')
@view('cmd_ref'
      '.html')
def get_cmd_ref():
    context = dict(default_context())
    context.update({'PAGE_ID': 'cmd_ref'})
    return context


@bottle.route('<:re:/docs/rest/overview/?>', method='GET')
@view('rest_overview.html')
def get_rest_overview():
    context = dict(default_context())
    context.update({'PAGE_ID': 'resto'})
    return context


@bottle.route('<:re:/docs/rest/ref/?>', method='GET')
@view('rest.html')
def get_rest_reference():
    context = dict(default_context())
    context.update({'PAGE_ID': 'restr',
                    'PUBLIC_HOST': utils.get_ec2_meta('/latest/meta-data/public-hostname/')})
    return context


@bottle.route('<:re:/docs/tutorials/start/?>', method='GET')
@view('getting_started.html')
def get_tutorials_getting_started():
    context = dict(default_context())
    context.update({'PAGE_ID': 'start'})
    return context


@bottle.route('<:re:/docs/tutorials/s3/?>', method='GET')
@view('aws_with_s3.html')
def get_tutorials_s3():
    context = dict(default_context())
    context.update({'PAGE_ID': 's3'})
    return context


@bottle.route('<:re:/settings/?>', method='GET')
@view('settings.html')
def get_settings():
    context = dict(default_context())
    context.update({'NAME': prefs.get('name', ''),
                    'EMAIL': prefs.get('email', ''),
                    'PASS': prefs.get('password', ''),
                    'END_POINT': prefs.get('proxy_cache_endpoints', ''),
                    'HOST': prefs.get('proxy_cache_redis_host', ''),
                    'PORT': prefs.get('proxy_cache_redis_port', ''),
                    'DBID': prefs.get('proxy_cache_redis_db', ''),
                    'RPASS': prefs.get('proxy_cache_redis_password', '')})
    return context


@bottle.route('<:re:/api/v1/?>', method='GET')
@view('api.html')
def api_get_handler():
    return get_rest_overview()


@bottle.route('<:re:/api/v1/optimize/?>', method='GET')
@view('api.html')
def optimize_get_handler():
    return get_rest_reference()


@bottle.route('/robots.txt', method='GET')
@view('robots.txt')
def robots_handler():
    return {}


@error(404)
@view('404.html')
def error404(err):
    context = dict(default_context())
    return context


@error(500)
@view('500.html')
def error500(err):
    context = dict(default_context())
    return context
