#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import json
import subprocess
from datetime import datetime
import time
import multiprocessing
import socket
import fileinput
import settings
import bottle
import requests
import re
from bottle import request, HTTPError
from common import utils
from htpasswd import HtpasswdFile
from . import jm, cache, prefs

try:
    import psutil
except:
    class psutil(object):
        @staticmethod
        def __getattr__(item):
            return 0 if item.startswith('get') else None


@bottle.route('<:re:/api/v1/heartbeat>', method='GET')
def heartbeat():
    """
    Verify server is alive
    """
    return {
        'time': datetime.utcnow().isoformat(),
        'jpegmini': {
            'core': {
                'version': jm.version(),
            },
            'api': {
                'version': utils.get_current_jm_api_version(),
            }
        },
    }


@bottle.route('<:re:/api/v1/info/lite?>', method='GET')
def get_info_lite_handler():
    """
    Get basic server info
    """
    return {
        'time': datetime.utcnow().isoformat(),
        'total_photos': int(prefs.get('jm_image_count', 0)),
        'total_saving': int(prefs.get('jm_bytes_saved', 0)),
        'restart_required': bool(prefs.get('restart_required', False)),
        'uptime_human': utils.seconds_to_human((time.time() - psutil.get_boot_time())),
    }


@bottle.route('<:re:/api/v1/info/?>', method='GET')
def get_info_handler():
    """
    Get extended server info
    This method should not be polled on constant basis to minimize overhead
    """
    phys_mem = psutil.phymem_usage()
    virt_mem = psutil.virtmem_usage()
    latest_version = utils.check_latest_version()

    if latest_version['jpegmini'] and latest_version['api']:
        update_available = utils.cmp_versions(latest_version['jpegmini'], jm.version()) == 1 or \
            utils.cmp_versions(latest_version['api'], utils.get_current_jm_api_version()) == 1
    else:
        update_available = False

    info = get_info_lite_handler()
    info.update({
        'time': datetime.utcnow().isoformat(),
        'jpegmini': {
            'core': {
                'version': jm.version(),
                'latest_version': latest_version['jpegmini'],
            },
            'api': {
                'version': utils.get_current_jm_api_version(),
                'latest_version': latest_version['api'],
            },
            'update_available': update_available,
        },
        'status': {
            'hostname': socket.gethostname(),
            'public-hostname': utils.get_ec2_meta('/latest/meta-data/public-hostname/'),
            'local-ip': utils.get_ec2_meta('/latest/meta-data/local-ipv4/'),
            'instance-id': utils.get_ec2_meta('/latest/meta-data/instance-id/'),
            'instance-type': utils.get_ec2_meta('/latest/meta-data/instance-type/'),
            'uptime': (time.time() - psutil.get_boot_time()),
            'cpu': {
                'cores': multiprocessing.cpu_count(),
                'usage': psutil.cpu_percent(),
            },
            'memory': {
                'ram': {
                    'total': phys_mem.total,
                    'free': phys_mem.free,
                    'used': phys_mem.used,
                    'percent': phys_mem.percent,
                },
                'virtual': {
                    'total': virt_mem.total,
                    'free': virt_mem.free,
                    'used': virt_mem.used,
                    'percent': virt_mem.percent,
                },
            }
        },
    })
    info['config'] = dict(((k, prefs.get(k)) for k in [
        'restart_required',
        'proxy_cache_backend',
        'proxy_cache_endpoints',
        'proxy_cache_redis_host',
        'proxy_cache_redis_port',
        'proxy_cache_redis_db',
        'proxy_cache_redis_password',
        'name',
        'email',
        'enable_cloudwatch']))

    info['cache'] = {
        'backend': info['config']['proxy_cache_backend'],
    }

    try:
        if info['cache']['backend'] == 'memcached':
            info['cache']['stats'] = cache.get_stats()
        elif info['cache']['backend'] == 'redis':
            info['cache']['stats'] = cache.info()
        else:
            info['cache']['stats'] = ''
    except:
        info['cache']['stats'] = ''

    return info


@bottle.route('/api/v1/config/<key>', method='GET')
def get_config_key(key):
    """
    Get the value of a config parameter
    If the parameter doesn't exist, null is returned
    """
    return json.dumps({'key': key, 'value': prefs.get(key)})


@bottle.route('/api/v1/config', method='GET')
def get_config_keys():
    """
    Get all config keys and values
    """
    config = prefs.list()
    latest_version = utils.check_latest_version()
    update_available = False
    if latest_version['jpegmini'] and latest_version['api']:
        update_available = utils.cmp_versions(latest_version['jpegmini'], jm.version()) == 1 or \
            utils.cmp_versions(latest_version['api'], utils.get_current_jm_api_version()) == 1
    config.update({'update_available': update_available})
    return json.dumps(config)


@bottle.route('/api/v1/config', method='POST')
def set_config_key():
    """
    Update settings in database

    New values should be sent in json using one of the following formats:
    Single value: { "key": "param_name", "value": param-value }
    Multiple values: [ { "key": "param_name", "value": param-value1 },
        { "key": "param_name2", "value": param-value2 },
        { "key": "param_name3", "value": param-value3 } ]

    To remove an attribute, set value to null, e.g.
        { "key": "param_name", "value": null }

    String values must be wrapped in quotes
    """
    try:
        items = json.loads(request.body.read())
        if type(items) == dict:
            items = [items]
        if type(items) == list:
            for item in items:
                key = item['key']
                value = item['value']
                if value is not None:
                    prefs.set(key, str(value))
                else:
                    prefs.remove(key)
                on_pref_change(key, value)
            return {'status': 'ok'}
    except ValueError:
        raise HTTPError(400, 'Expected object with key/value or an array of such objects')
    except KeyError as ex:
        raise HTTPError(400, 'Missing key %s' % ex.message)


@bottle.route('/api/v1/restart', method='POST')
def restart_service():
    """
    Restart nginx and reload gunicorn

    This is required after updating password, or updating code
    Most configuration changes do not require an update

    The 'restart_required' config parameter will be set to True
    if a restart is required
    """
    subprocess.Popen(['/opt/jpegmini/www/scripts/restart'])
    return {'status': 'ok'}


@bottle.route('/api/v1/update', method='POST')
def update_code():
    """
    Run update script, to get latest version of code from bitbucket
    The update script will also restart the server
    """
    subprocess.Popen(['/opt/jpegmini/www/scripts/update'])
    return {'status': 'ok'}


def on_pref_change(key, value):
    """
    Handle changes in parameters.
    """
    # register server with jpegmini.com
    if key in ['email', 'name']:
        data = {'email': prefs.get('email'),
                'name': prefs.get('name'),
                'document': utils.get_ec2_meta('/latest/dynamic/instance-identity/document/')}
        requests.post('http://www.jpegmini.com/app_services/aws/register', data=data)

    # update nginx password file, or if password is empty disable auth in nginx config
    #if key == 'password':
    #    new_password = value
    #    if new_password:
    #        htpasswd = HtpasswdFile(settings.HTPASSWORD_FILE, create=True)
    #        htpasswd.update('admin', new_password)
    #        htpasswd.save()
    #        for line in fileinput.FileInput("/etc/nginx/conf.d/jpegmini.conf", inplace=1):
    #            line = re.sub("#*auth_basic", "auth_basic", line)
    #            print line,
    #    else:
    #        for line in fileinput.FileInput("/etc/nginx/conf.d/jpegmini.conf", inplace=1):
    #            line = re.sub("auth_basic", "#auth_basic", line)
    #            print line,

    if key == 'proxy_cache_backend':
        prefs.set('proxy_cache_backend', value.lower())

    # check if restart is required
    if key in ['proxy_cache_backend', 'proxy_cache_endpoints', 'proxy_cache_redis_host',
               'proxy_cache_redis_port', 'proxy_cache_redis_db', 'proxy_cache_redis_password', 'enable_cloudwatch']:
        prefs.set('restart_required', True)
