#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import urllib
import bottle
import requests
import json
from bottle import request, response, HTTPError
from hashlib import sha1
from . import jm, cache, prefs
from boto.ec2.cloudwatch import CloudWatchConnection

try:
    _cwc = CloudWatchConnection() if prefs.get('enable_cloudwatch') in ['True', 'true'] else None
except:
    _cwc = None


def post_cwc(metrics):
    if _cwc:
        try:
            for m in metrics:
                name, unit, value = m
                _cwc.put_metric_data(namespace="JPEGmini",name=name,unit=unit,value=value)
        except:
            return False
    return True


@bottle.route('<:re:/api/v1/optimize/?>', method='POST')
def optimize_post_handler():
    """
    Recompress handler
    The body of the request is the JPEG data

    Optional request parameters (pass in url):
    scale - scale factor, float between 0 and 1
    width - target width, in pixels
    height - target height, in pixels

    * If no parameters are provided, the image size will be maintained (scale=1.0)
    * You cannot pass both scale and (width or height)
    * If only width, or only height are provided, the original aspect ration is maintained
    """
    scale = request.query.get('scale', None)
    width = request.query.get('width', None)
    height = request.query.get('height', None)

    if scale and (width or height):
        raise HTTPError(400, 'Cannot specify target scale and (width or height)')

    if scale:
        try:
            scale = float(scale)
            assert 0 < scale <= 1
        except:
            raise HTTPError(400, 'Scale must be a float, and 0 < scale <= 1')

    if width:
        try:
            width = int(width)
            assert 0 < width
        except:
            raise HTTPError(400, 'width must be an integer, and 0 < width <= src_width, where src_width is the width of the input image')

    if height:
        try:
            height = int(height)
            assert 0 < height
        except:
            raise HTTPError(400, 'height must be an integer, and 0 < height <= src_height, where src_height is the height of the input image')

    res = 0
    try:
        response.headers['Content-Type'] = 'image/jpeg'
        jm.set_image(request.body.read())
        res = jm.optimize(scale=scale, width=width, height=height)

        savings = jm.get_info()['size']-len(res['data'])
        prefs.incr('jm_image_count', 1)
        prefs.incr('jm_bytes_saved', savings)
        if not post_cwc([('photos', 'Count', 1.0), ('savings', 'Bytes', savings)]):
            response.headers['x-jpegmini-cloudwatch'] = 'error posting metrics'
        return res['data']
    except Exception as ex:
        raise HTTPError(400, str(ex))
    finally:
        jm.free_image()



@bottle.route('<:re:/api/v1/proxy/?>', method='GET')
def proxy_get_handler():
    url = request.query.get('url', None)
    if not url:
        raise HTTPError(400, 'Missing url')

    url = urllib.unquote(url).decode('utf8')
    url_hash = sha1(url).hexdigest()
    mini_content = cache.get(url_hash)
    if mini_content is not None:
        meta = cache.get(url_hash+'_meta')
        if meta:
            j = json.loads(meta)
            src_size = j['src']
            mini_size = j['mini']
            response.headers['x-jpegmini-src'] = str(src_size)
            response.headers['x-jpegmini-mini'] = str(mini_size)
            response.headers['x-jpegmini-reduced'] = str(src_size-mini_size)
            response.headers['x-jpegmini-reduced_p'] = str(1.0-1.0*mini_size/src_size)
            prefs.incr('jm_bytes_saved', src_size-mini_size)
        response.headers['Content-Type'] = 'image/jpeg'
        response.headers['x-jpegmini-hit'] = 'true'
        return mini_content

    r = requests.get(url)
    if not (200 <= r.status_code < 300):
        raise HTTPError(r.status_code, 'Error getting %s' % url)

    response.headers['Content-Type'] = 'image/jpeg'
    response.headers['x-jpegmini-hit'] = 'false'
    try:
        src_size = len(r.content)
        jm.set_image(r.content)
        res = jm.optimize()
        cache.set(url_hash, res['data'])
        mini_size = len(res['data'])
        response.headers['x-jpegmini-src'] = str(src_size)
        response.headers['x-jpegmini-mini'] = str(mini_size)
        response.headers['x-jpegmini-reduced'] = str(src_size-mini_size)
        response.headers['x-jpegmini-reduced_p'] = str(1.0-1.0*mini_size/src_size)
        cache.set(url_hash+'_meta', json.dumps({'src': src_size, 'mini': mini_size}))
        prefs.incr('jm_image_count', 1)
        prefs.incr('jm_bytes_saved', src_size-mini_size)
        if not post_cwc([('photos', 'Count', 1.0), ('savings', 'Bytes', src_size-mini_size)]):
            response.headers['x-jpegmini-cloudwatch'] = 'error posting metrics'
        return res['data']
    except Exception as ex:
        response.headers['x-jpegmini-error'] = str(ex)
        return r.content
    finally:
        jm.free_image()

