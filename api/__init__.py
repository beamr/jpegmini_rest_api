#
# JPEGmini REST API
# Copyright (c) 2013 ICVT
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import libpyjm
import logging
import traceback
from common.preferences import Preferences

class MockCache(object):
    """
    Mock Cache is not a cache. It just implements the cache get/set api and does nothing
    """
    def set(self, key, value):
        return value

    def get(self, key):
        return None


def _init_cache():
    """
    Return a cache object (memcache / redis / Mock) depending on cache settings
    """
    cache_backend = prefs.get('proxy_cache_backend')
    cache_ = MockCache()
    try:
        if cache_backend == 'memcached':
            import memcache
            cache_endpoints = prefs.get('proxy_cache_endpoints')
            cache_ = memcache.Client(cache_endpoints.split(','))

        elif cache_backend == 'redis':
            import redis
            host = prefs.get('proxy_cache_redis_host')
            port = prefs.get('proxy_cache_redis_port')
            db = prefs.get('proxy_cache_redis_db')
            password = prefs.get('proxy_cache_redis_password')
            cache_ = redis.StrictRedis(host=host, port=int(port), db=db, password=password)
    except Exception as ex:
        logging.warning('Unable to initialize cache: %s' % traceback.format_exc())
    return cache_


# Initialize JPEGmini library
jm = libpyjm.PyJM()
prefs = Preferences()
prefs.set('restart_required', False)

cache = _init_cache()

